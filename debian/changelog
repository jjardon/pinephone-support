pinephone-support (0.2.8) unstable; urgency=medium

  * d/control: replace `zram-tools` with `systemd-zram-generator`
  * d/control: add `u-boot-menu`
  * d/gbp.conf: make mobian the default branch

 -- Arnaud Ferraris <aferraris@debian.org>  Thu, 09 Feb 2023 18:01:15 +0100

pinephone-support (0.2.7) unstable; urgency=medium

  * d/control: update dependencies and switch to 6.1 kernel

 -- Arnaud Ferraris <aferraris@debian.org>  Tue, 13 Dec 2022 15:22:18 +0100

pinephone-support (0.2.6) unstable; urgency=medium

  * d/control: move to Debian package for RTL8723 firmware.
    The firmware for the RTL8723CS has been uploaded to Debian as
    `firmware-realtek-rtl8723cs-bt`, let's use that one.
    This commit also updates various fields to acknowledge the move to
    Salsa and changes the package architecture to `all` so we get CI
    pipelines to pass (it doesn't contain any machine-specific code after
    all).

 -- Arnaud Ferraris <aferraris@debian.org>  Fri, 05 Aug 2022 12:17:23 +0200

pinephone-support (0.2.5) unstable; urgency=medium

  * Revert "d/control: depend on `u-boot-sunxi`"
    This reverts commit 61248b566474da6d888836220470898e031d36f5 as Tow-Boot
    actually *does* include `crust`.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Mon, 04 Apr 2022 19:09:01 +0200

pinephone-support (0.2.4) unstable; urgency=medium

  * d/control: depend on `u-boot-sunxi`
    Despite previously planning to switch to user-installed Tow-Boot as the
    bootloader for this device, we need to revert back to using
    `u-boot-sunxi` as the former lacks support for `crust`. This change will
    be reverted as soon as this issue is fixed.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Mon, 04 Apr 2022 13:31:26 +0200

pinephone-support (0.2.3) unstable; urgency=medium

  * d/control: update dependencies.
    USB gadget config moved from mobian-tweaks-common to
    mobile-usb-networking, so make sure we don't lose it on the next
    upgrade. Moreover, we're moving to promoting Tow-Boot instead of
    embedding the bootloader in the image itself, so drop the dependency on
    u-boot-sunxi.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Thu, 24 Mar 2022 16:46:53 +0100

pinephone-support (0.2.2) unstable; urgency=medium

  * d/control: switch to 5.15 kernel
  * debian: add gbp.conf

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 20 Nov 2021 13:49:58 +0100

pinephone-support (0.2.1) unstable; urgency=medium

  * d/control: don't depend on arm-trusted-firmware

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 19 Jan 2021 18:27:46 +0100

pinephone-support (0.2.0) unstable; urgency=medium

  * d/control: switch to 5.10 kernel

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 06 Jan 2021 20:59:12 +0100

pinephone-support (0.1.9) unstable; urgency=medium

  * d/control: drop dependency on pinephone-modem-scripts

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 14 Dec 2020 16:41:45 +0100

pinephone-support (0.1.8) unstable; urgency=medium

  * d/control: drop dependency on u-boot-menu.
    It is now a dependency to mobian-pinephone-tweaks.
  * d/control: depend on eg25-manager.
    This package will soon replace pinephone-modem-scripts, so make sure
    we're ready for that.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 10 Dec 2020 15:14:35 +0100

pinephone-support (0.1.7) unstable; urgency=medium

  * d/control: depend on mobian-pinephone-tweaks

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 04 Nov 2020 17:20:58 +0100

pinephone-support (0.1.6) unstable; urgency=medium

  * d/control: upgrade to 5.9 kernel.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 03 Nov 2020 11:01:30 +0100

pinephone-support (0.1.5) unstable; urgency=medium

  * d/control: add dependency on OV5640 firmware for camera AF

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 06 Oct 2020 15:15:20 +0200

pinephone-support (0.1.4) unstable; urgency=medium

  * d/control: upgrade to 5.8 kernel and use package wys-sunxi

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 19 Sep 2020 10:31:11 +0200

pinephone-support (0.1.3) unstable; urgency=medium

  * remove pinephone-devtools package

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 02 Sep 2020 19:58:38 +0200

pinephone-support (0.1.2) unstable; urgency=medium

  * pinephone-support: add dependency on kernel package

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 25 Jun 2020 12:59:13 +0200

pinephone-support (0.1.1) unstable; urgency=medium

  * pinephone-devtools: fix usb-gadget service name

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 14 May 2020 03:19:27 +0200

pinephone-support (0.1.0) unstable; urgency=medium

  * Initial release

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 07 May 2020 15:40:17 +0200
